class PicturesController < ApplicationController
  before_action :authenticate_user!, :except => [:index, :big_picture]
  before_action :set_picture, only: [:show, :edit, :update, :destroy, :big_picture]


  def index
    @pictures = user_signed_in? ? current_user.pictures : Picture.all
  end

  def show
  end

  def new
    @picture = Picture.new
  end

  def edit 
  end

  def update
    respond_to do |format|
      if @picture.update(picture_params)
        format.html { redirect_to picture_path(@picture), notice: 'The Picture was successfully updated.' }
        format.json { render :show, status: :ok, location: @picture }
      else
        format.html { render :edit }
        format.json { render json: @picture.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @picture.destroy
    respond_to do |format|
      format.html { redirect_to pictures_path, notice: 'The picture was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def create
    @picture = Picture.new(picture_params)
    @picture.user = current_user

    respond_to do |format|
      if @picture.save
        format.html { redirect_to pictures_path, notice: 'The Picture was successfully added.' }
        format.json { render :index, status: :created, location: @picture }
      else
        format.html { render :new }
        format.json { render json: @picture.errors, status: :unprocessable_entity }
      end
    end
  end

  def big_picture
    p params
    respond_to do |format|
      format.js
      format.html
    end
  end
  
  private 

  def set_picture
    @picture = Picture.find(params[:id])
  end

  def picture_params
    params.require(:picture).permit(:description, :file)
  end
end

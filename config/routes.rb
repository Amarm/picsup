Rails.application.routes.draw do
  devise_for :users
  resources :pictures do 
    get :big_picture, :on => :member
  end

  root :to => "pictures#index"
end
